//import {Schema, model} from 'mongoose';
import pkg from 'mongoose';
const { Schema, model } = pkg;

const Notes = new Schema({
    text: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

export default Notes;