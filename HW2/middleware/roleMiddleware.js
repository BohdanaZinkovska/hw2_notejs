import pkg from 'jsonwebtoken';
const { verify } = pkg;
import { secret } from '../config.js';


export default function roleMiddleware(roles) {
    return function(req, res, next) {
        if (req.method === 'OPTIONS') {
            next();
        }
        try {
            const token = req.headers.authorization.split(' ')[1];
            if (!token) {
                return res.status(403).json({ message: 'Please logIn' });
            }
            const { roles: userRole } = verify(token, secret);
            let hasRole = false;
            userRole.forEach((role) => {
                if (roles.includes(role)) {
                    hasRole = true;
                }
            });
            if (!hasRole) {
                return res.status(403).json({ message: 'You can\'t get it' });
            }
            next();
        } catch (e) {
            console.log(e);
            return res.status(403).json({ message: 'Please, logIn' });
        }
    };
}