import pkg from 'jsonwebtoken';
const { verify } = pkg;
import { secret } from '../config.js';

export default function(req, res, next) {
    if (req.method === 'OPTIONS') {
        next();
    }
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(403).json({ message: 'Please logIn' });
        }
        const decoder = verify(token, secret);
        req.user = decoder;
        next();
    } catch (e) {
        console.log(e);
        return res.status(403).json({ message: 'Please, logIn' });
    }
}