module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true
    },
    "extends": [
        "google"
    ],
    "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 12
    },
    "rules": {
        "require-jsdoc": 0
    }
}