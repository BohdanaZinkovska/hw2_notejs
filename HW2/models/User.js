import pkg from 'mongoose';
const { Schema, model } = pkg;
import Notes from './Notes.js';

const User = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    roles: [{
        type: String,
        ref: 'Role',
    }],
    posts: [Notes],
});

export default model('User', User);