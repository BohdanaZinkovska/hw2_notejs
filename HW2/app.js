const hostname = 'localhost';
const PORT = 8080;
import express, { json } from 'express';
import pkg from 'mongoose';
const { connect } = pkg;
const app = express();
import router from './authRouter.js';

app.use(json());
app.use('/api', router);


async function begin() {
    try {
        await connect('mongodb+srv://Bohdana:1326@cluster0.k078o.mongodb.net/hw2NodeJS');
        app.listen(PORT, hostname, () => {
            console.log(`Server running at http://${hostname}:${PORT}/`);
        });
    } catch (err) {
        console.log(err);
    }
}

begin();