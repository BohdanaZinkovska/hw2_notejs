import User from './models/User.js';
import Role from './models/Role.js';
import Notes from './models/Notes.js';
import pkg from 'bcryptjs';
const { hashSync, compareSync } = pkg;
import pkgj from 'jsonwebtoken';
const { sign } = pkgj;
import { validationResult } from 'express-validator';
import { secret } from './config.js';

const generateToken = (id, roles) => {
    const payload = {
        id,
        roles,
    };
    return sign(payload, secret, { expiresIn: '12h' });
};

export default class AuthCollect {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ message: 'Go home', errors });
            }
            const { username, password } = req.body;
            const isUser = await User.findOne({ username });
            if (isUser) {
                return res.status(400)
                    .json({ message: 'User have already regisrated.' });
            }
            const userRole = await Role.findOne({ value: 'USER' });
            const hashPass = bcrypt.hashSync(password, 7);
            const user = new User({
                username,
                password: hashPass,
                roles: [userRole.value],
            });
            await user.save();
            return res.json({ message: 'Success! User created!' });
        } catch (e) {
            res.status(400).json({ message: 'Error of registration' });
            console.log(e);
        }
    }
    async login(req, res) {
        try {
            const { username, password } = req.body;
            const user = await User.findOne({ username });
            if (!user) {
                res.status(400).json({ message: 'No such user' });
            }
            const validPass = bcrypt.compareSync(password, user.password);
            if (!validPass) {
                res.status(400).json({ message: 'Wrong password' });
            }
            const token = generateToken(user._id, user.roles);
            return res.json({ token });
        } catch (e) {
            res.status(400).json({ message: 'Error of login' });
            console.log(e);
        }
    }
    async getUsers(req, res) {
        try {
            const users = await User.find();
            res.json(users);
        } catch (e) {
            console.log(e);
        }
    }
    async makeNotes(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ message: 'Go home', errors });
            }
            const { note } = req.body;
            const makeNote = new Notes({ note });
            await makeNote.save();
            return res.json({ message: 'Success! Note created!' });
        } catch (e) {
            res.status(400).json({ message: 'Error' });
            console.log(e);
        }
    }
    async getNotes(req, res) {
        try {
            const note = await Notes.find();
            res.json(note);
        } catch (e) {
            console.log(e);
        }
    }
    async getNotesByUser(req, res) {
        try {
            const note = await Notes.find(user._id);
            res.json(note);
        } catch (e) {
            console.log(e);
        }
    }
    async getMe(req, res) {
        try {
            const { username } = req.body;
            const myAcc = await User.findOne({ username });
            if (!myAcc) {
                res.status(400).json({ message: 'No such user' });
            }
            const myId = generateToken(myAcc._id);
            return res.json({ myId });
        } catch (e) {
            res.status(400).json({ message: 'Error of login' });
            console.log(e);
        }
        const users = await User.find();
        res.json(users);
    } catch (e) {
        console.log(e);
    }
}

//export new AuthCollect();