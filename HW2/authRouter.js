import express, { Router } from 'express';
import pkg from './authCollect.js'
const { login, registration, getUsers, makeNotes, getNotes } = pkg;
import { check } from 'express-validator';
// import authMiddleware from './middleware/authMiddleware';
import roleMiddleware from './middleware/roleMiddleware.js';

const router = new Router();
router.post('/auth/login', function(req, res) { login });
router.post('/auth/registration', [
    check('username', 'Required field').notEmpty(),
    check('password', 'Lenght must be more 4 symbols - max 10')
    .isLength({ min: 4, max: 10 }),
], function(req, res) { registration });
router.get('/users', roleMiddleware(['USER']), function(req, res) { getUsers });
router.post('/notes', function(req, res) { makeNotes });
router.get('/notes', function(req, res) { getNotes });
//router.get('/users/me', getMe);
//router.patch('/users/me', getMe);
//router.get('/users/me', getMe);


export default router;